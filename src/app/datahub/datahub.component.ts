import { Component, OnInit } from '@angular/core';
import { Transpodata } from '../domain/transpodata'
import { Sensordata } from '../domain/sensordata'
import { TansposerviceService } from '../service/tansposervice.service';
import { from } from 'rxjs';
import {MessageService} from 'primeng/api';
declare var google: any;

interface City {
  name: string,
  code: string
}

interface SensorType {
  name: string,
  code: string
}

interface SensorLocation {
  name: string,
  code: string
}

interface Coverage {
  name: string,
  code: string
}
 

@Component({
  selector: 'app-datahub',
  templateUrl: './datahub.component.html',
  styleUrls: ['./datahub.component.css'],
  providers: [MessageService]
})
export class DatahubComponent implements OnInit {

  transportation: Transpodata[];

  sensors: Sensordata[];

  options: any;

  overlays: any[];

  cities: City[];
  type: SensorType[];
  location: SensorLocation[];
  coverage: Coverage[];
  rangeDates: Date[];

  selectedCity: City;
  selectedSensorType: SensorType;
  selectedLocation: SensorLocation;
  selectedCoverage: Coverage;

  selectedPosition: any;
  selectedLat: any;
  selectedLong: any;

  newRequest: boolean;
  sensor: Sensordata = {};
  displayDialog: boolean;
  displayDataDialog: boolean;
  successDialog:boolean;

  constructor(private transposervice: TansposerviceService) {

    this.cities = [
      {name: 'General Santos City', code: 'GSC'}
  ];

    this.type = [
      {name: 'CCTV Camera', code: 'cctv'}
  ];

    this.location = [
      {name: 'Digos - Makar Rd - E. Bulaong Ave Intersection ', code: 'dmre'}
  ];

    this.coverage = [
      {name: 'Daily', code: 'dly'},
      {name: 'Weekly', code: 'wkly'},
      {name: 'Monthly', code: 'mnthly'},
      {name: 'Yearly', code: 'yrly'}
  ];

  }

  ngOnInit(): void {
    this.transposervice.getTranspodata().then(transportation => this.transportation = transportation);
    this.transposervice.getSensordata().then(sensors => this.sensors = sensors);

    this.options = {
      center: {lat: 6.119734, lng:  125.178564},
      zoom: 12
    };

    this.overlays = [
      new google.maps.Marker({position: {lat:6.118644 , lng:125.162047 }, title:"Digos - Makar Rd - E. Bulaong Ave Intersection " , icon: "assets/sensor.png"}),
      new google.maps.Marker({position: {lat:6.119183 , lng:125.187769 }, title:"Digos Makar Rd - Jose Catolico Ave Intersection" , icon: "assets/sensor.png"}),
      new google.maps.Marker({position: {lat:6.118692 , lng:125.187272 }, title:"Digos Makar Rd - Jose Catolico Ave Intersection" , icon: "assets/sensor.png"}),
      new google.maps.Marker({position: {lat:6.120936 , lng:125.189364 }, title:"Digos Makar Rd - Honorio Arriola St. Intersection" , icon: "assets/sensor.png"}),
      new google.maps.Marker({position: {lat:6.118802 , lng:125.171342 }, title:"Salvani - Apparante St Intersection" , icon: "assets/sensor.png"}),
      new google.maps.Marker({position: {lat:6.119236 , lng:125.181525 }, title:"Salvani - Leon Llido St Intersection" , icon: "assets/sensor.png"}),
      new google.maps.Marker({position: {lat:6.127176 , lng:125.19565  }, title:"Digos Makar Rd. - NLSA Road (Lagao Public Market)" , icon: "assets/sensor.png"}),
      new google.maps.Marker({position: {lat:6.118797 , lng:125.170128 }, title:"Digos - Makar Rd - Pendatun Ave Intersection" , icon: "assets/sensor.png"}),
      new google.maps.Marker({position: {lat:6.118233 , lng:125.170197 }, title:"Pendatun Ave - Pres JP Laurel Ave Intersection" , icon: "assets/sensor.png"}),
      new google.maps.Marker({position: {lat:6.118142 , lng:125.17325  }, title:"Pres JP Laurel Ave - Roxas Ave Intersection" , icon: "assets/sensor.png"}),
      new google.maps.Marker({position: {lat:6.118825 , lng:125.173378 }, title:"Digos - Makar Rd - E. Roxas Ave Intersection" , icon: "assets/sensor.png"}),
      new google.maps.Marker({position: {lat:6.118989 , lng:125.179478 }, title:"Digos - Makar Rd - Santiago Blvd Intersection" , icon: "assets/sensor.png"}),
      new google.maps.Marker({position: {lat:6.118792 , lng:125.179633 }, title:"Digos - Makar Rd - Santiago Blvd Intersection" , icon: "assets/sensor.png"}),
      new google.maps.Marker({position: {lat:6.118703 , lng:125.179581 }, title:"Pres JP Laurel Ave - Santiago Blvd Intersection" , icon: "assets/sensor.png"}),
      new google.maps.Marker({position: {lat:6.118961 , lng:125.187106 }, title:"Digos Makar Rd - Jose Catolico Ave Intersection" , icon: "assets/sensor.png"}),
      ];        
  }

  // handleOverlayClick() {
  //   let bounds = new google.maps.LatLngBounds();
  //   this.overlays.forEach(marker => {
  //     bounds.extend(marker.getPosition());
  // });

  //   console.log(bounds);
  // }

  handleOverlayClick(event: any) {
    let isMarker = event.overlay.getTitle != undefined;

    if (isMarker) {
        this.selectedPosition = event.overlay.getTitle();
        // this.infoWindow.setContent('' + title + '');
        // this.infoWindow.open(event.map, event.overlay);
        this.selectedLat = event.overlay.getPosition();
        event.map.setCenter(event.overlay.getPosition());
        console.log(this.selectedLat);
        // this.messageService.add({severity:'insfo', summary:'Marker Selected', detail: title});
    }
    else {
        // this.messageService.add({severity:'info', summary:'Shape Selected', detail: ''});
    }
  }

  handleMapClick(event) {
    this.selectedLat = event.latLng;
  }

  showDialogToRequest() {
    this.newRequest = true;
    this.sensor = {};
    this.displayDialog = true;
  }

  showDialogForViewData(){
    this.displayDataDialog = true;
  }

  showSuccessDiloag(){
    this.displayDialog = false;
    this.successDialog = true;
  }

  request() {
    let sensora = [...this.sensors];
    sensora.push(this.sensor);
    this.displayDialog = false;
  }

  cancel() {
    this.displayDialog = false;
} 

}
