import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Transpodata } from '../domain/transpodata';
import { Sensordata } from '../domain/sensordata'
import { from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TansposerviceService {
  //private baseurl1 = 'http://103.29.250.148:81/tomasdatahub-api/api/v1';
  private baseurl = 'http://103.29.250.173:8080/genapi1/gscmart/api';
  private baseurl1 = 'http://103.29.250.173:8080/tomasdatahub-api/api/v1';

  constructor(private http: HttpClient) { }

  getTranspodata() {
    return this.http.get<any>('assets/transpodata.json')
    .toPromise()
    .then(res => <Transpodata[]>res.data)
    .then(data => { return data; });
  }

  getSensordata() {
    return this.http.get<any>('assets/sensordata.json')
    .toPromise()
    .then(res => <Sensordata[]>res.data)
    .then(data => { return data; });
  }

  getAllDatasets(){
    return this.http.get<Array<any>>(this.baseurl + '/tables');
  }

  getMyRequest(id: any){
    return this.http.get<Array<any>>(this.baseurl1 + '/myrequest/' + id);
  }

  createRequest(id: any , data: any , status: any){
    return this.http.post(this.baseurl1 + '/create', {
      user_id: id , data: data , status: status
    }).toPromise().then((data: any) => {
      console.log("request sent");
    })
  }

  cancelRequest(rid: any , uid: any){
    return this.http.post(this.baseurl1 + '/cancel', {
      request_id: rid , user_id: uid,
    }).toPromise().then((data: any) => {
      console.log("request cancel");
    })
  }

  dataDownload(tablename: any){
    return this.http.get<Array<any>>(this.baseurl1 + '/data/' + tablename);
  }
}
