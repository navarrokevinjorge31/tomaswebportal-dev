import { Component, OnInit } from '@angular/core';
import { TansposerviceService } from '../service/tansposervice.service'
import {MenuItem} from 'primeng/api';
import { Router } from '@angular/router';

interface City {
  name: string,
  code: string
}

@Component({
  selector: 'app-hub',
  templateUrl: './hub.component.html',
  styleUrls: ['./hub.component.css']
})
export class HubComponent implements OnInit {

  selectedCity: City;
  cities: City[];
  public dataset: any;
  public req: any;
  public request_id: any;
  public factdataset: any;
  public datarequest: any;
  public download_data: any;
  public columns: any;
  cols: any[];
  fcols: any[];
  interval: any;

  displayDataDialog: boolean;
  displayConfirmRequest: boolean;
  displayConfirmCancel: boolean;
  isButtonDownloadVisible = true;
  visible: boolean = true;
  items: MenuItem[];

  constructor(private tansposerviceService: TansposerviceService , private router: Router) { }

  home(){
    this.router.navigateByUrl('/');
  }

  hub(){
    this.router.navigateByUrl('/hub');
  }
  
  apigen(){
    window.open('http://103.29.250.173:8080/apigenv1/');
  }


  ngOnInit(): void {

    this.items = [
      {
          label:'Home',
          icon:'pi pi-fw pi-home',
          command: () => this.home(),

      },
      {
          label:'Datahub',
          icon:'pi pi-fw pi-folder-open',
          command: () => this.hub(),
      },
      {
          label:'Api Generator',
          icon:'pi pi-fw pi-clone',
          command: () => this.apigen(),
      },
      {
          label:'Management',
          icon:'pi pi-fw pi-user',
          
      }
  ];

    this.getAllDatasets();
    this.getMyRequest(1);
    
    this.interval = setInterval(() => { 
      this.getMyRequest(1);
  }, 5000);

    this.cities = [
      {name: 'General Santos City', code: 'GSC'}
    ];

    this.cols = [
      { field: 'fn', header: 'Filename' },
      { field: 'stat', header: 'Status' },
      { field: 'act', header: 'Action' }
    ];

    this.fcols = [
      { field: 'col', header: 'Column' },
      { field: 'com', header: 'Comments' }
    ]
  }

  getAllDatasets(){
    this.tansposerviceService.getAllDatasets().subscribe(res => {
      this.dataset = res;
      
    })
  }

  getMyRequest(id: any){
    this.tansposerviceService.getMyRequest(id).subscribe(res => {
      this.datarequest = res;
    }) 
  }

  requestData(data: any){
    this.tansposerviceService.createRequest('1', data , 'pending');
    this.displayConfirmRequest = false;
  }

  requestCancel(rid: any){
    this.tansposerviceService.cancelRequest(rid,'1');
    this.displayConfirmCancel = false;
  }

  downloadRequest(tablename: any){
    this.tansposerviceService.dataDownload(tablename).subscribe(res => {
      this.download_data = res['request'];
      this.download(tablename);
    });
  }

  download(tablename: any){
    var csvData = this.ConvertToCSV(this.download_data);
    var a = document.createElement("a");
    a.setAttribute('style', 'display:none;');
    document.body.appendChild(a);
    var blob = new Blob([csvData], { type: 'text/csv' });
    var url= window.URL.createObjectURL(blob);
    a.href = url;
    var x:Date = new Date();
    var link:string = tablename + '.csv';
    a.download = link.toLocaleLowerCase();
    a.click();

  }

  ConvertToCSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';
    var row = "";

    for (var index in objArray[0]) {
        row += index + ',';
    }
    row = row.slice(0, -1);

    str += row + '\r\n';

    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if (line != '') line += ','

            line += array[i][index];
        }
        str += line + '\r\n';
    }
    return str;
  }

  //dialogs
  showDialogForViewData(table_name: string){
    this.displayDataDialog = true;
    this.factdataset = table_name;
  }

  showConfirmRequestDialog(requested: any){
    this.displayConfirmRequest = true;
    this.req = requested;
  }

  showCancelRequestDialog(rid: any){
    this.request_id = rid;
    this.displayConfirmCancel = true;
  }

}
