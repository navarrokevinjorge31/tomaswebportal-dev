import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';
import { PhotoService } from '../service/photo.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  images: any[];

  items: MenuItem[];

  constructor(private photoService: PhotoService , private router: Router) { }

  responsiveOptions:any[] = [
    {
        breakpoint: '1024px',
        numVisible: 5
    },
    {
        breakpoint: '768px',
        numVisible: 3
    },
    {
        breakpoint: '560px',
        numVisible: 1
    }
];

  ngOnInit(): void {

    this.items = [
      {
          label:'Home',
          icon:'pi pi-fw pi-home',
          command: () => this.home(),

      },
      {
          label:'Datahub',
          icon:'pi pi-fw pi-folder-open',
          command: () => this.hub(),
      },
      {
          label:'Api Generator',
          icon:'pi pi-fw pi-clone',
          command: () => this.apigen(),
      },
      {
          label:'Management',
          icon:'pi pi-fw pi-user',
          
      }
  ];

  this.photoService.getImages().then(images => this.images = images);

  console.log(this.images)

  }

  home(){
    console.log("test")
  }

  hub(){
    this.router.navigateByUrl('/hub');
  }

  apigen(){
    window.open('http://103.29.250.173:8080/apigen/');
  }

}
