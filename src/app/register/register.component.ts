import { Component, OnInit } from '@angular/core';

interface Yourself {
  name: string,
  id: string
}

interface Organization{
  name: string,
  id: string
}

interface Industry{
  name: string,
  id: string
}


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  yourself: Yourself[];

  selectedYourself: Yourself;

  industry: Industry[];

  selectedIndustry: Industry;

  organization: Organization[];

  selectedOrg: Organization;

  recaptcha: any[];

  constructor() { 

    this.yourself = [
      {name: 'Analyt/Researcher/Specialist', id: '1'},
      {name: 'C-Level Executive', id: '2'},
      {name: 'Consultant', id: '3'},
      {name: 'Developer', id: '4'},
      {name: 'Educator', id: '5'},
      {name: 'Elected/Appointer Official', id: '6'},
      {name: 'Engineer', id: '7'},
      {name: 'GIS Analyst', id: '8'},
      {name: 'GIS Manager/Supervisor', id: '9'},
      {name: 'IT Manager/Superviser', id: '10'},
      {name: 'IT Technician', id: '11'},
      {name: 'Manager/Supervisor (Non-GIS/IT)', id: '12'},
      {name: 'Other', id: '13'},
      {name: 'Sr. Executive', id: '14'},
      {name: 'Student', id: '15'}
    ];

    this.industry = [
      {name: 'Adminstration', id: '1'},
      {name: 'Agriculture', id: '2'},
      {name: 'Archeology', id: '3'},
      {name: 'Architecture/Engineering/Construction', id: '4'},
      {name: 'Assesor/Cadastral Records', id: '5'},
      {name: 'Banking/Credit Unions/Financial Services', id: '6'},
      {name: 'Business Services', id: '7'},
      {name: 'Cartography', id: '8'},
      {name: 'Convervation', id: '9'},
      {name: 'Defense/Intelligence', id: '10'},
      {name: 'Demographics/Census/Elections', id: '11'},
      {name: 'Electric/Gas Utility', id: '12'},
      {name: 'Environment Management', id: '13'},
      {name: 'Fire', id: '14'},
      {name: 'Fishiries and Wildlife', id: '15'},
      {name: 'Forestry', id: '16'},
      {name: 'GIS Services', id: '17'},
      {name: 'Health and Human Services', id: '18'},
      {name: 'Higher Education', id: '19'},
      {name: 'Homeland Security', id: '20'},
      {name: 'Insurance', id: '21'},
      {name: 'K-12 Education', id: '22'},
      {name: 'Law Enforcement', id: '23'},
      {name: 'Library/Museum', id: '24'},
      {name: 'Location-based Services', id: '25'},
      {name: 'IT Technician', id: '26'},
      {name: 'Media Publishing', id: '27'},
      {name: 'Mining', id: '28'},
      {name: 'Natural Resources', id: '29'},
      {name: 'Others', id: '30'},
      {name: 'Petroleum', id: '31'},
      {name: 'Pipeline', id: '32'},
      {name: 'Planning/Economic Development', id: '33'},
      {name: 'Public Safety/Emergency Development', id: '34'},
      {name: 'Public Works', id: '35'},
      {name: 'Real Estate', id: '36'},
      {name: 'Retail/Commercial Business', id: '37'},
      {name: 'Survey', id: '38'},
      {name: 'Telecommunication', id: '39'},
      {name: 'Transportation/Fleet Management', id: '40'},
      {name: 'Water Resources', id: '41'},
      {name: 'Water/Waste Water/Stormwater', id: '42'}
    ];

    this.organization = [
      {name: 'City/Town Government', id: '1'},
      {name: 'Commercial/Private Business', id: '2'},
      {name: 'Country Government', id: '3'},
      {name: 'Education-Staff/Faculty', id: '4'},
      {name: 'Education-Student', id: '5'},
      {name: 'Non-Profit Organization', id: '6'},
      {name: 'Regional Agency/Goverment', id: '7'}
    ];

  }

  ngOnInit(): void {
  }

  resolved(captchaResponse: any[]){
    this.recaptcha = captchaResponse;
    console.log(this.recaptcha);
  }

}
