import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule , routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';

import {CardModule} from 'primeng/card';
import {InputTextModule} from 'primeng/inputtext';
import {DropdownModule} from 'primeng/dropdown';
import {FormsModule} from '@angular/forms';
import {CaptchaModule} from 'primeng/captcha';
import {ButtonModule} from 'primeng/button';
import { TableModule } from 'primeng/table';
import { HttpClientModule } from '@angular/common/http';
import {GMapModule} from 'primeng/gmap';
import {CalendarModule} from 'primeng/calendar';
import {DialogModule} from 'primeng/dialog';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { HubComponent } from './hub/hub.component';
import { HomeComponent } from './home/home.component'
import {SidebarModule} from 'primeng/sidebar';
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatMenuModule} from '@angular/material/menu';
import { AboutComponent } from './about/about.component';
import { MainComponent } from './main/main.component';
import { MenubarModule } from 'primeng/menubar';
import {GalleriaModule} from 'primeng/galleria';



@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    HubComponent,
    HomeComponent,
    AboutComponent,
    MainComponent,
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CardModule,
    InputTextModule,
    DropdownModule,
    FormsModule,
    CaptchaModule,
    ButtonModule,
    TableModule,
    HttpClientModule,
    GMapModule,
    CalendarModule,
    DialogModule,
    NgxDocViewerModule,
    SidebarModule,
    FlexLayoutModule,
    MatMenuModule,
    MenubarModule,
    GalleriaModule,
    
    
    
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

