import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DatahubComponent } from './datahub/datahub.component';
import { HubComponent } from './hub/hub.component';
import { from } from 'rxjs';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { MainComponent } from './main/main.component';

const routes: Routes = [
  { path: '' , component:MainComponent},
  { path: 'home' , component:HomeComponent},
  { path: 'login' , component: LoginComponent},
  { path: 'register' , component: RegisterComponent},
  { path: 'datahub' , component: DatahubComponent},
  { path: 'hub' , component: HubComponent},
  { path: 'about' , component: AboutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [MainComponent , HomeComponent ,  LoginComponent , RegisterComponent , DatahubComponent , HubComponent , AboutComponent]
